function CreateCustomerAdapter(customer) {
	return {
		email: customer.email,
		address: { country: customer.countryCode ?? null },
	};
}

module.exports = {
	CreateCustomerAdapter,
};

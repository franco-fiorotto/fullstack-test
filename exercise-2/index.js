const Stripe = require('stripe');
const {
	CreateCustomerAdapter,
} = require('./src/adapters/createCustomerAdapter');
const { GetCountry } = require('./utils/getCountry');
const GetResponseMessage = require('./utils/getResponseMessage');
const WriteFile = require('./utils/writeFile');

const STRIPE_TEST_SECRET_KEY =
	'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR';
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

const finalCustomersFilePath = './final-customers.json';
const customers = require('./customers.json');

const handler = async (country) => {
	try {
		//Getting country object by 'country name' or 'country code'. Both are accepted.
		const countryObject = GetCountry(country);
		if (!countryObject) {
			console.log(GetResponseMessage('COUNTRY_NOT_FOUND'));
			return;
		}

		if (!customers || customers.length === 0) {
			console.log(GetResponseMessage(CUSTOMER_NO_DATA));
			return;
		}

		//1- Filtering the customers (in customers.json) by country
		const filteredCustomersByCountry = customers.filter(
			(customer) => customer.country === countryObject.name
		);

		if (filteredCustomersByCountry.length === 0) {
			console.log(
				GetResponseMessage('NO_CUSTOMERS_FOR_COUNTRY'),
				': ',
				countryObject.name
			);
			return;
		}

		//2- Transforming the filtered customers to save into Stripe
		//3- For each transformed customer create a Stripe customer.
		const customersCreationPromises = [];
		filteredCustomersByCountry.forEach((customer) => {
			//Creating customer object for Stripe
			//Here we have two approaches:
			//1. Expanding customer object adding a countryCode property.
			//2. Passing two parameters to CreateCustomerAdapter, customer and countryCode.
			//This time I've chosen the first approach because it's flexible in case we need to add more properties
			//and also because it keeps the adapter signature cleaner keeping in mind a better reusability.
			customer.countryCode = countryObject.code;
			const customerToCreate = CreateCustomerAdapter(customer);

			const customerCreatePromise = stripe.customers.create(customerToCreate);

			customersCreationPromises.push(customerCreatePromise);
		});

		//4- Stripe customer objects array with email, country and id as properties.
		//Awaiting for all promises to be resolved. This is to paralelize the user creation.
		const customersCreated = await Promise.all(customersCreationPromises);

		//5- Writing stripe customers array into final-customers.json using fs
		WriteFile(finalCustomersFilePath, customersCreated);

		console.log(customersCreated);
	} catch (e) {
		console.error(GetResponseMessage('CUSTOMER_FETCH_ERROR'), ': ', e.message);
	}
};

handler('ES');

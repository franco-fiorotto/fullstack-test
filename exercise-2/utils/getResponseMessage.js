const responses = require('./responsesMessages.json');
const responsesStringified = JSON.stringify(responses);
const responsesParsed = JSON.parse(responsesStringified);

module.exports = function GetResponseMessage(responseCode) {
	return responsesParsed[responseCode.toUpperCase()] ?? 'Error';
};

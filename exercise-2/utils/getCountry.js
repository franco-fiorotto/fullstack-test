const countries = require('../countries-ISO3166.json');
const countriesStringified = JSON.stringify(countries);
const countriesParsed = JSON.parse(countriesStringified);

function GetCountryNameByCode(countryCode) {
	if (!countryCode) {
		return '';
	}

	return countriesParsed[countryCode.toUpperCase()];
}

function GetCountryCodeByName(countryName) {
	if (!countryName) {
		return '';
	}

	return Object.keys(countriesParsed).find(
		(key) => countriesParsed[key] === countryName
	);
}

function GetCountry(country) {
	const countryName = GetCountryNameByCode(country);
	if (countryName) {
		const countryCode = GetCountryCodeByName(countryName);
		return { code: countryCode, name: countryName };
	}

	const countryCode = GetCountryCodeByName(country);
	if (countryCode) {
		const countryName = GetCountryNameByCode(countryCode);
		return { code: countryCode, name: countryName };
	}
	return;
}

module.exports = { GetCountry };

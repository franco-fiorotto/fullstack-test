const fs = require('fs');

module.exports = function WriteFile(route, data) {
	fs.writeFile(
		route,

		JSON.stringify(data),

		function (err) {
			if (err) {
				console.error(`Error while writing: ${err.message}`);
				throw err;
			}
		}
	);
};

import { gql } from '@apollo/client';
const GET_POSTS = gql`
	{
		user(id: 1) {
			id
			name
			phone
			email
			company {
				name
			}
			address {
				street
				suite
				city
			}
		}
		posts(options: { slice: { limit: 10 } }) {
			data {
				id
				title
				body
			}
		}
	}
`;
const PostsServices = { GET_POSTS };
export default PostsServices;

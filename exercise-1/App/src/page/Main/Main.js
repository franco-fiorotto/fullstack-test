import { useQuery } from '@apollo/client';
import React from 'react';
import PostsComponent from '../../components/PostsComponent/PostsComponent';
import PostsServices from './services/posts';
import UserComponent from '../../components/UserComponent/UserComponent';
import Loading from '../../components/LoadingComponent/Loading';
import ErrorComponent from '../../components/ErrorComponent/ErrorComponent';
import MainPageContext from './contexts/MainPageContext';

export default function Main() {
	const { loading, error, data } = useQuery(PostsServices.GET_POSTS);

	if (loading) return <Loading />;

	if (error) return <ErrorComponent error={error} />;

	return (
		<MainPageContext.Provider
			value={{
				posts: data?.posts?.data,
				user: data?.user,
			}}
		>
			<UserComponent />
			<PostsComponent />
		</MainPageContext.Provider>
	);
}

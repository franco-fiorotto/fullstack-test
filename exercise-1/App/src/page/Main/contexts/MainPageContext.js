import { createContext } from 'react';

const initialData = {};
const MainPageContext = createContext({
	data: initialData,
});

export default MainPageContext;

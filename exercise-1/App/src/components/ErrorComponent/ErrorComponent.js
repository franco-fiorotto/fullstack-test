import React from 'react';

const ErrorComponent = ({ error }) => {
	let { message } = error;
	if (!message) {
		message = 'Something went wrong';
	}
	return (
		<div className="m-auto">
			<div className="text-white font-bold text-4xl mb-12">
				Error
				<span role="img" aria-label="sad face">
					😢
				</span>
			</div>
			<div className="text-white text-2xl mb-12">{message}</div>
		</div>
	);
};

export default ErrorComponent;

import React from 'react';
import BackgroundImg from '../../assets/images/bg.jpg';

const Layout = ({ children }) => {
	return (
		<div
			className="font-sans antialiased text-gray-900 leading-normal tracking-wider bg-cover p12"
			style={{
				backgroundImage: `url(${BackgroundImg})`,
				backgroundPosition: 'center',
				backgroundSize: 'cover',
				backgroundRepeat: 'no-repeat',
			}}
		>
			<div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
				{children}
			</div>
		</div>
	);
};

export default Layout;

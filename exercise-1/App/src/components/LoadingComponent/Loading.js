import React from 'react';

const Loading = ({ text }) => {
	let textToDisplay = '';
	if (text) {
		textToDisplay = text;
	}
	return (
		<div className="text-white text-3xl flex justify-center m-auto">
			<span role="img" aria-label="Loading">
				⏳
			</span>
			Loading {textToDisplay}...
		</div>
	);
};

export default Loading;

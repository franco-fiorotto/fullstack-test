import { createContext } from 'react';

const initialData = [];
const PostsContext = createContext({
	postsContextData: initialData,
	setPostsContextData: () => {},
	filteredPostsContextData: initialData,
	setFilteredPostsContextData: () => {},
});

export default PostsContext;

import React from 'react';

export default function PostsHeader({ userName }) {
	const name = userName ?? 'Unknown';

	return (
		<p className="pt-4 text-base font-bold flex items-center justify-center m-auto lg:justify-start mb-4">
			{name}'s Posts:
		</p>
	);
}

import React, { useContext, useEffect, useState } from 'react';
import MainPageContext from '../../../../page/Main/contexts/MainPageContext';
import SearchComponent from '../../../SearchComponent/SearchComponent';
import PostsContext from '../../contexts/postsContext';

export default function Search() {
	const [searchValue, setSearchValue] = useState('');
	const { setFilteredPostsContextData } = useContext(PostsContext);
	const { posts } = useContext(MainPageContext);

	useEffect(() => {
		const filteredPosts = posts.filter((post) => {
			return post.title.toLowerCase().includes(searchValue.toLowerCase());
		});
		setFilteredPostsContextData(filteredPosts);
	}, [searchValue, posts, setFilteredPostsContextData]);

	const searchValueHandler = (e) => {
		setSearchValue(e.target.value);
	};

	return (
		<div className="mb-3 w-full">
			<SearchComponent
				value={searchValue}
				onChange={searchValueHandler}
				placeholder="Search by title..."
			/>
		</div>
	);
}

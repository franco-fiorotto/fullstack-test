import React from 'react';

const Card = (props) => {
	const { title = 'Undefined', body = 'No body' } = props;
	return (
		<div className="border-solid rounded-lg border-2 mb-2 flex flex-col px-4 py-2">
			<h5 className="text-gray-900 font-bold text-lg mb-6">Title: {title}</h5>
			<p className="text-gray-900 text-md mb-6">{body}</p>
		</div>
	);
};

export default Card;

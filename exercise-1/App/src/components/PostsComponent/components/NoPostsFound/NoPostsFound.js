import React from 'react';

export default function NoPostsFound() {
	return (
		<div className="flex justify-center pt-3 pb-12">
			<h3 className="text-gray-700 text-xl font-semibold">No posts found</h3>
		</div>
	);
}

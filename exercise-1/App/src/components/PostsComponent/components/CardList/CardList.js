import React, { useContext } from 'react';
import PostsContext from '../../contexts/postsContext';
import Card from '../Card/Card';
import NoPostsFound from '../NoPostsFound/NoPostsFound';

const CardList = () => {
	const { filteredPostsContextData } = useContext(PostsContext);
	if (filteredPostsContextData.length === 0) {
		return <NoPostsFound />;
	}

	return filteredPostsContextData.map((post) => {
		return <Card key={post.id} title={post.title} body={post.body} />;
	});
};

export default CardList;

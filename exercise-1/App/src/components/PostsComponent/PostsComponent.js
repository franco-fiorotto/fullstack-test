import React, { useContext, useState } from 'react';
import MainPageContext from '../../page/Main/contexts/MainPageContext';
import CardList from './components/CardList/CardList';
import PostsHeader from './components/PostsHeader/PostsHeader';
import Search from './components/Search/Search';

import PostsContext from './contexts/postsContext';

const PostsComponent = () => {
	const { posts, user } = useContext(MainPageContext);
	const [filteredPostsContextData, setFilteredPostsContextData] =
		useState(posts);

	return (
		<div className="w-full lg:w-2/5 mx-6 lg:mx-0 h-screen py-12">
			<div className="w-full rounded-lg lg:rounded-r-lg lg:rounded-l-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0 px-12 h-full overflow-auto">
				<PostsHeader userName={user?.name} />
				<PostsContext.Provider
					value={{
						filteredPostsContextData,
						setFilteredPostsContextData,
					}}
				>
					<Search />
					<CardList />
				</PostsContext.Provider>
			</div>
		</div>
	);
};

export default PostsComponent;

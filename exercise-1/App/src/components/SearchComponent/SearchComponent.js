import React from 'react';

export default function SearchComponent({ value, onChange, placeholder }) {
	return (
		<input
			type="search"
			alt="search"
			value={value}
			onChange={onChange}
			placeholder={placeholder}
			className="w-full px-3 py-1.5 text-gray-700 border border-solid rounded hover:bg-gray-200 focus:border-blue-600 focus:outline-none"
		/>
	);
}

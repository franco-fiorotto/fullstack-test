import React, { useContext } from 'react';
import MainPageContext from '../../page/Main/contexts/MainPageContext';

export default function UserComponent() {
	const { user } = useContext(MainPageContext);
	const {
		name = 'Not found',
		address = { street: 'Not found', suite: 'Not found', city: 'Not found' },
		email = 'Not found',
		phone = 'Not found',
		company = { name: 'Not found' },
	} = user;

	return (
		<div
			id="profile"
			className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0"
		>
			<div className="p-4 md:p-12 text-center lg:text-left">
				<h1 className="text-3xl font-bold pt-8 lg:pt-0">{name}</h1>
				<div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
				<p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
					Adress:
				</p>
				<p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
					{address.street} - {address.suite} - {address.city}
				</p>
				<p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
					Email:
				</p>
				<p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
					{email}
				</p>
				<p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
					Phone:
				</p>
				<p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
					{phone}
				</p>
				<p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
					Company:
				</p>
				<p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
					{company.name}
				</p>
			</div>
		</div>
	);
}

import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import Main from './page/Main/Main';
import Layout from './components/LayoutComponent/Layout';

const client = new ApolloClient({
	uri: 'https://graphqlzero.almansi.me/api',
	cache: new InMemoryCache(),
});

function App() {
	return (
		<ApolloProvider client={client}>
			<Layout>
				<Main />
			</Layout>
		</ApolloProvider>
	);
}

export default App;
